package ajaymehta.hashset1collection;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

// Same as previous prgram but ..printing is done in only 1 method.. for mcore info see  --> MethodOverloadingVsGeneriVsObjectMethod Project..

public class Program2 {

    public static void main(String args[]) {

        Set<String> hs = new java.util.HashSet<>();

        // add elements in hashSet

        hs.add("One");
        hs.add("Two");
        hs.add("Three");
        hs.add("Four");  // adding duplicate of four ..lets see if it prints duplicate or not..
        hs.add("Four");
        hs.add("Four");
        hs.add("Four");
        hs.add("Five");

        // view the hashSet ( no order)
        print(hs);

        // you are just viewing ..if you iterate then then u can use each n every element from hasSet ..see in the next Program2


        //===================================================================

        // Checking the size of HashSet ...
       print(hs.size());

        //=====================================================================

        // iterate hasSet using for each loop
        for (String i : hs) {
          print(i);
        }

        System.out.println("====================================================");

        //========================================================================

        // iterate hasSet using iterator...
        Iterator it = hs.iterator();

        while (it.hasNext()) {

            String elements = (String) it.next();
           print(elements);
        } // end of while loop

        //=====================================================================

        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
        // lets add an element to hashSet (using add method that returns boolen value)  ...if that element is already in hasSet ..then this method will return false n element will not be added
        // othewise  element will be added n ..it will return true ...

        print(hs.add("Twenty"));
        print(hs.add("Five"));

        // then print hashSet
       print(hs);

        //===================================================================


        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");

        // lets remove the elements from hashSet with true and false value....

        print(hs.remove("Twenty"));    // if element avail then it will remove n return true
        print(hs.remove("Hundred")); // if element is not avail in hasSet then it will return false..

        // then print hashSet
        System.out.println(hs);

        //===================================================================

            // lets check if hashSet contain particular element or not .. it returns value in boolean

        boolean check = hs.contains("Five");

       print(check);



        //===================================================================



        // To clear hashSet ..we have clear method

        // TODO ...uncomment below to see....how it works..
      /*   hs.clear();
        System.out.println("See its cleared -->  "+hs);*/

        //===================================================================











    } // end of main method..

    // tried of writing System.out.println() ..let me put it in a method..whenever i need it i will call this method..

    public static <T>void print(T value) {

        System.out.println(value);
    }



}
