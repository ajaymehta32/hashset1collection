package ajaymehta.hashset1collection;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by Avi Hacker on 7/17/2017.
 */

// The elements of a Set have no order, so you can't access elements by index ..if u need to access elements by index using Arraylist instead


public class Program3 {

    public static void main(String args[]) {

        HashSet hs = new HashSet(Arrays.asList("One","Two","Three","Four","Five"));  // avoid raw types

        HashSet<String> hs2 = new HashSet(Arrays.asList("One","Two","Three","Four","Five"));

        HashSet<Integer> hs3 = new HashSet(Arrays.asList(1,2,3,4,5,6));

        System.out.println(hs);
        System.out.println(hs2);
        System.out.println(hs3);

    }
}
