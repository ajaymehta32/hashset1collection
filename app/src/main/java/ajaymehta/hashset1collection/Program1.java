package ajaymehta.hashset1collection;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

// The elements of a Set have no order, so you can't access elements by index ..if u need to access elements by index using Arraylist instead


// HashSet implements Set interface ...if we need to use set anywhere ..we create hasSet (Collecton of Objects)

// Two main properties of hasSet
// 1. No order of elements ...elements can be in any order ..a total messed up structure...
// 2. It does not store duplicate elements..


    // hey listen commented all System.out .line  n pass all values in a print method..

public class Program1 {
    // doesnt matter what hasSet method return ... we can use those method according to our needs like  ..if(hs.isEmpty()) --> then do this , if(hs.contains("element") --> then do this.. n so on..

    public static void main(String args[]) {

        Set<String> hs = new java.util.HashSet<>();

        // add elements in hashSet

        hs.add("One");   // it is the same add method that return boolean value..but we dont need to see any result..we just want
        hs.add("Two");
        hs.add("Three");
        hs.add("Four");  // adding duplicate of four ..lets see if it prints duplicate or not..
        hs.add("Four");
        hs.add("Four");
        hs.add("Four");
        hs.add("Five");

        // view the hashSet ( no order)
       System.out.println(hs);


        // you are just viewing ..if you iterate then then u can use each n every element from hasSet ..see in the next Program2


        //===================================================================

        // Checking the size of HashSet ...
        System.out.println(hs.size());

        if(hs.size() == 0) {

            System.out.println("Put some elements in hashSet");
        } else {
            System.out.println("We have elements in hashSet");
        }

        //=====================================================================

        // iterate hasSet using for each loop
        for (String i : hs) {
            System.out.println(i);
        }

        System.out.println("====================================================");

        //========================================================================

        // iterate hasSet using iterator...
        Iterator it = hs.iterator();

        while (it.hasNext()) {

            String elements = (String) it.next();
            System.out.println(elements);
        } // end of while loop

        //=====================================================================

        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
        // lets add an element to hashSet (using add method that returns boolen value)  ...if that element is already in hasSet ..then this method will return false n element will not be added
        // othewise  element will be added n ..it will return true ...

        System.out.println(hs.add("Twenty"));
        System.out.println(hs.add("Five"));

        // then print hashSet
        System.out.println(hs);

        //===================================================================


        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");

        // lets remove the elements from hashSet with true and false value....

        System.out.println(hs.remove("Twenty"));    // if element avail then it will remove n return true
        System.out.println(hs.remove("Hundred"));   // if element is not avail in hasSet then it will return false..

        // then print hashSet
        System.out.println(hs);

        //===================================================================

            // lets check if hashSet contain particular element or not .. it returns value in boolean

        System.out.println("Checking if element is avail in HashSet or Not..");
        boolean check = hs.contains("Fuve");  // case senstive ...beware..

        boolean check2 = hs.contains("FiveHundred");

        System.out.println(check);
        System.out.println(check2);

        // you can do other operation like..

        if(hs.contains("One")) {
            hs.remove("One");
        }

        System.out.println(hs);


        //===================================================================

        System.out.println("Checking if hashSet is empty of not ..");

        // return true if hashSet has no elements..

        System.out.println(hs.isEmpty());  // false coz  hasSet has Elements ..

        // you dont always need boolean value it returns.u can use these methods in other ways also like see below

        if(!hs.isEmpty()) {

            hs.add("Kalia");
        }
        System.out.println(hs);

        //===================================================================



        // To clear hashSet ..we have clear method

        // TODO ...uncomment below to see....how it works..
         hs.clear();
        System.out.println("See its cleared -->  "+hs);

        //===================================================================






    } // end of main method..


}
