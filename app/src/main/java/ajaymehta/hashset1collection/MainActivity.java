package ajaymehta.hashset1collection;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    // out tak is to put names in hashSet n print it..

    TextView tv, tv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.tv_mytv);
        tv2 = (TextView) findViewById(R.id.tv_mytv2);

        // this is just example of how to utilize collection to a upper level...
        Hashset hashset = new Hashset();  // oobject of our defined Hashset class ...

        hashset.add("ajay");
        hashset.add("rahul");
        hashset.add("kalia");

        tv.setText(hashset.print());

        //=====================================================================


        // Lets do it object way..



        // this is just example of how to utilize collection to a upper level...
        Hashset2 hashset2 = new Hashset2();  // oobject of our defined Hashset class ...

        //now we can send Integer type , String ,Float object  or mixture objects too....see below mixture example

        hashset2.add("YOYOY");
        hashset2.add(123);
        hashset2.add(4.4f);
        hashset2.add("TIme paaa");

        //=======================================

        // it returns String ..so check "YES" or "NO"
        if(hashset2.contain("Delhi").equalsIgnoreCase("NO")) {  // it is return string ..so we have to check (our string == with return value) ..if no then add it..

            hashset2.add("Delhi");

        }

        //===========================================

        hashset2.delete(123);

        // lets print..

        tv2.setText(hashset2.print());


    }
}
