package ajaymehta.hashset1collection;

import java.util.HashSet;

/**
 * Created by Avi Hacker on 7/17/2017.
 */



public class Hashset2<T> {



    HashSet<Object> hs = new HashSet();

    public void add(Object element) {
        if (!hs.contains(element)) {
            hs.add(element);
        }

    }


    //==========================================

    public void deleteAll() {

        hs.clear();
    }

    //==========================================

    public void delete(Object element) {

        if(hs.contains(element)) {

            hs.remove(element);
        }

    }

    //==========================================


    public int isEmpty() {

        if(hs.isEmpty()) {  // our defined ..if  hasSet is empty it will return 1  else it will return 0
            return 1;
        } else {
            return 0;
        }
    }

    //===========================================


      public String contain(Object element) {

        if(hs.contains(element)) {
            return "YES";
        }
        else {
            return "NO";
        }

    }


    public StringBuilder print() {  // take care its a collection HashSet ..not our  Hashset..

        StringBuilder sb = new StringBuilder();

        for(Object s : hs) {

            sb.append(s+"\n");
        }
        return sb;
    }


}
