package ajaymehta.hashset1collection;

import  java.util.HashSet;

/**
 * Created by Avi Hacker on 7/17/2017.
 */

// The elements of a Set have no order, so you can't access elements by index ..if u need to access elements by index using Arraylist instead

    // This is just example file you can do this with all type of collection n there gonna be so many meaningful purpose for this..like when  creating shopping cart..
// This file tells how to use HashSet or any other collection in your whole project....
    // just create a file and put all the methods of HashSet ..here ..n whereEver you need it ..create object of this class
    // n access its method .... lets see the example ...using MainActivity <--

public class Hashset {

    // take care of 1 thing ...  our class name and our collection  (HasSet ) have bit difference in name Hashset (Class) - HashSet (collection ) <-- so take care of them ...if they both have same name
    // then u have to take HashSet like -->   java.util.HashSet<String>  hs =

    HashSet<String> hs = new HashSet();

    public void add(String element) { // this is just example of String elements ..if you want to restrict elements of
                                      // String n integer type ..then you can do Method Overloading ..see below..
        if (!hs.contains(element)) {
            hs.add(element);
        }

    }

    // create a different file for Integer Paramiterzed type..cool
    // TODO..uncomment below to know you cant do method overloading with a Single Paraetrized object .. it only
    // takes string value   HashSet<String> hs = new HashSet<String>();
    // so our object is  -- > hs  ( and all method having this object to add,remove value) from this object only
    // so if we create object for ... HashSet<Integer> hs2  --> then we have to create all the methods again with having object hs2

    // so ...method overoading fails here ..for parametrized type..go  generic method or Object method ..for the solution
    // lets see the solution is  Hashset2 file  (object style)


 // Method overloading..
 /*   public void add(Integer element) { // this is just example of String elements ..if you want to restrict elements of
        // String n integer type ..then you can do Method Overloading ..see below..
        if (!hs.contains(element)) {
            hs.add(element);
        }

    }
*/

    //==========================================

    public void deleteAll() {

        hs.clear();
    }

    //==========================================

    public void delete(String element) {

        if(hs.contains(element)) {

            hs.remove(element);
        }

    }

    //==========================================


    public int isEmpty() {

        if(hs.isEmpty()) {  // our defined ..if  hasSet is empty it will return 1  else it will return 0
            return 1;
        } else {
            return 0;
        }
    }

    //===========================================


    public String contain(String element) {

        if(hs.contains(element)) {
            return "YES";
        }
        else {
            return "NO";
        }

    }


    public StringBuilder print() {  // take care its a collection HashSet ..not our  Hashset..

        StringBuilder sb = new StringBuilder();

        for(Object s : hs) {

            sb.append(s+"\n");
        }
        return sb;
    }


}
