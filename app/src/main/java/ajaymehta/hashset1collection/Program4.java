package ajaymehta.hashset1collection;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Avi Hacker on 7/17/2017.
 */

// convert set  into Array..

public class Program4 {

    public static void main(String args[]) {


       Set<String> hs = new HashSet<>(Arrays.asList("One","Two","Three","Four","Five","Six","Six"));
        hs.add("Eight");

        System.out.println(hs);
        //====================================


        Object [] stringArray =  hs.toArray();  // create a object type array from Set<String> n printed it..

        for(Object i : stringArray) {
            System.out.println(i);
        }
    }




}
